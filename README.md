# Ansible ubuntu 18.04 TIG-Stack Deployment

This Setup deploys a influxDB, Telegraf & Grafana Setup at a Ubuntu 18.04 Remote Server

# Use
Add IP-Adress of remote Server in the inventory File.

Make sure, that you have root Access to your remote Machine over SSH.

This is a Demo Setup, if you run it in Production, change the Passwords of InfluxDB and Telegrag in the .yml file.

After running, you can Access the Grafana Admin Dashboard over serverip:3000, login is admin:admin. Please change on first login.

To build the Telegraf Dashboard, add in Grafana InfluxDB as DataSource with the Database Name, User and Password (default=telegraf:telegraf). Import a Dashboard and use '5955' as ID. After this, you have a runnig Setup.

